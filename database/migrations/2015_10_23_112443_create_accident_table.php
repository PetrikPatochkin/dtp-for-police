<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accidents', function (Blueprint $table){
            $table->increments('id');
            $table->dateTime('date');
            $table->tinyInteger('status')->default(0);
            $table->float('lng',9,7);
            $table->float('lat',9,7);

            $table->index('lng');
            $table->index('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accidents');
    }
}
