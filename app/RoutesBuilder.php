<?php

namespace App;

use App\Address;
use Illuminate\Support\Facades\Input;

use Ivory\GoogleMap\Services\Directions\Directions;
use Ivory\GoogleMap\Services\Directions\DirectionsRequest;
use Ivory\GoogleMap\Services\Base\TravelMode;
use Ivory\GoogleMap\Services\Base\UnitSystem;
use Ivory\GoogleMap\Services\Directions\DirectionsResponse;
use Widop\HttpAdapter\CurlHttpAdapter;

use Polyline as GooglePolylineCoder;

class RoutesBuilder {
    const MarkerError = 0.00007;//7.5 metres
    const UnitSystem = UnitSystem::METRIC;
    const TravelMode = TravelMode::DRIVING;
    const Language = 'uk';

    public function initKeyPoint(array $addresses_id = []){
        $directions = new Directions(new CurlHttpAdapter());
        $request = new DirectionsRequest();
        // Build directions for request
        $addresses = Address::whereIn('id', $addresses_id)->get();
        $addresses_cnt = count($addresses);
        if($addresses_cnt<2) return false;
        foreach($addresses as $address){
            switch( array_search($address->id, $addresses_id) ){
                case 0:
                    $request->setOrigin((float)$address->lat, (float)$address->lng, true);
                    break;
                case $addresses_cnt-1:
                    $request->setDestination((float)$address->lat, (float)$address->lng, true);
                    break;
                default:
                    $request->addWaypoint((float)$address->lat, (float)$address->lng, true);
            }
        }
        $request->setProvideRouteAlternatives(true);
        $request->setTravelMode(self::TravelMode);
        $request->setUnitSystem(self::UnitSystem);
        $request->setLanguage(self::Language);
        //$request->setOptimizeWaypoints(true);

        $response = $directions->route($request);
        return $response;
    }

    public function makeRoutes(DirectionsResponse $response){
        $routes = [];
        foreach($response->getRoutes() as $response_route){
            $route = new \stdClass();
            $route->keyStreet = $response_route->getSummary();
            $route->distance = 0;
            $route->timeSpan = 0;


            $route->routeCoordinates = $this->buildRouteCoordinates($response_route);

            $accidentsInfo = $this->accidentsInfoOnRoute($route->routeCoordinates);
            $route->accidents = $accidentsInfo['accidents'];
            $route->accidentsCount = count($route->accidents);
            $route->victimsCount = $accidentsInfo['victimsCount'];
            $route->deathsCount = $accidentsInfo['deathsCount'];
            $route->dangerPoints = $accidentsInfo['dangerPoints'];

            $route->navigation = [];

            foreach($response_route->getLegs() as $response_leg){
                $route->distance += $response_leg->getDistance()->getValue();
                $route->timeSpan += $response_leg->getDuration()->getValue();
                foreach($response_leg->getSteps() as $response_step){
                    $navigation_direct = new \stdClass();
                    $navigation_direct->distance = $response_step->getDistance()->getValue();
                    $navigation_direct->comment = $response_step->getInstructions();
                    preg_match("#<.*?>(.*?)<\/.*>#", $navigation_direct->comment, $direct);
                    if(!isset($direct[1])) $direct[1] = '';
                    switch( $direct[1] ){
                        case "ліворуч":
                        case "left":
                            $navigation_direct->type = 'left';
                            break;
                        case "праворуч":
                        case "right":
                            $navigation_direct->type = 'right';
                            break;
                        default:
                            $navigation_direct->type = 'forward';
                    }
                    $route->navigation[] = $navigation_direct;
                }
            }
            $routes[] = $route;
        }
        $this->setDangerColorsToRoutes($routes);
        return $routes;
    }

    protected function buildRouteCoordinates($route){
        $coordinates = [];
        $encodedRoutePolyline = $route->getOverviewPolyline()->getValue();
        $points = GooglePolylineCoder::Decode($encodedRoutePolyline);
        $pairs = GooglePolylineCoder::Pair($points);
        foreach($pairs as $coordinate){
            $point = new  \stdClass();
            $point->lat = $coordinate[0];
            $point->lng = $coordinate[1];
            $coordinates[] = $point;
        }
        return $coordinates;
    }

    protected function accidentsInfoOnRoute($routeCoordinates){
        // Select possible accidents in rectangle area
        $accidents = [];
        $routeDangerPoints = 0;
        $routeVictimsCount = 0;
        $routeDeathsCount = 0;

        // Find accidents on parts of route to optimize system load
        $routePartsCoordinates = array_chunk($routeCoordinates, 25);
        foreach ($routePartsCoordinates as $routePartCoordinates) {
            // Chunking array remove paths between them, so we need to restore it
            if(isset($last_p)) array_unshift($routePartCoordinates, $last_p);

            $accidents_query = new Accident();
            $routesCnt = count($routePartCoordinates);
            for($i=1; $i<$routesCnt; $i++){
                $p1 = $routePartCoordinates[$i-1];
                $p2 = $routePartCoordinates[$i];
                $accidents_query = $accidents_query
                    ->orWhere(function($query) use ($p1,$p2){
                        $query->where('lat', '>', min($p1->lat,$p2->lat)-self::MarkerError);
                        $query->where('lat', '<', max($p1->lat,$p2->lat)+self::MarkerError);
                        $query->where('lng', '>', min($p1->lng,$p2->lng)-self::MarkerError);
                        $query->where('lng', '<', max($p1->lng,$p2->lng)+self::MarkerError);
                    });
            }
            $last_p = clone $p2;
            $possibleAccidents = $accidents_query
                ->where('date','>',date("Y-m-d",strtotime('-1 year')))
                ->get();
            foreach($possibleAccidents as $accident){
                for($i=1; $i<$routesCnt; $i++){
                    $p1 = $routePartCoordinates[$i-1];
                    $p2 = $routePartCoordinates[$i];
                    if( $accident->lat < min($p1->lat,$p2->lat)-self::MarkerError
                        || $accident->lat > max($p1->lat,$p2->lat)+self::MarkerError
                        || $accident->lng < min($p1->lng,$p2->lng)-self::MarkerError
                        || $accident->lng > max($p1->lng,$p2->lng)+self::MarkerError)
                        continue;
                    if($this->isDotOnLineBetween2Points($accident, $p1, $p2, self::MarkerError)){
                        $accident->accidentsCount = 1;
                        switch($accident->status){
                            case 1:
                                $accident->victimsCount = 1;
                                $routeVictimsCount++;
                                $accident->deathsCount = 0;
                                $routeDangerPoints += 1;
                                break;
                            case 2:
                                $accident->victimsCount = 1;
                                $routeVictimsCount++;
                                $accident->deathsCount = 1;
                                $routeDeathsCount++;
                                $routeDangerPoints += 2;
                                break;
                        }
                        $accident->lat = (float)$accident->lat;
                        $accident->lng = (float)$accident->lng;
                        $accidents[$accident->id] = $accident;
                        break;
                    }
                }
            }
        }
        //return $accidents;
        return [
            'accidents' => array_values($accidents),
            'victimsCount' => $routeVictimsCount,
            'deathsCount' => $routeDeathsCount,
            'dangerPoints' => $routeDangerPoints
        ];
    }

    protected function isDotOnLineBetween2Points($dot, $point1, $point2, $error=0.00001/*~1 meter in degree*/){
        // Calculating A,B,C of the equation of the line
        $x2mx1 = $point2->lat - $point1->lat;
        $y2my1 = $point2->lng - $point1->lng;

        if($x2mx1 == 0) return abs($dot->lat - $point1->lat) <= $error;
        if($y2my1 == 0) return abs($dot->lng - $point1->lng) <= $error;

        $A = 1/$x2mx1;
        $B = -1/$y2my1;
        $C = $point1->lat/(-$x2mx1) + $point1->lng/$y2my1;

        $distance = abs($A*$dot->lat + $B*$dot->lng + $C) / sqrt($A*$A + $B*$B);

        return $distance <= $error;
    }

    protected function setDangerColorsToRoutes(&$routes){
        usort($routes, function($a, $b){
            if($a->dangerPoints == $b->dangerPoints) return 0;
            return $a->dangerPoints < $b->dangerPoints ? -1 : 1;
        });
        foreach($routes as $index=>$route){
            unset($route->dangerPoints);
            switch($index){
                case 0:
                    $route->safetyLevel = 'green';
                    break;
                case 1:
                    $route->safetyLevel = 'yellow';
                    break;
                case 2:
                    $route->safetyLevel = 'red';
                    break;
            }
        }
    }
}