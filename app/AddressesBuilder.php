<?php

namespace App;

class AddressesBuilder {

    const Region = 'ua';
    const Language = 'uk';
    protected $geocoder = NULL;

    public function __construct() {
        $this->geocoder = new \GoogleMapsGeocoder();
        $this->geocoder
            ->setRegion(self::Region)
            ->setLanguage(self::Language);
    }

    public function setAddress($term){
        $this->geocoder
            ->setAddress($term);
    }

    public function setCoordinates($lat, $lng){
        $this->geocoder
            ->setLatitude($lat)
            ->setLongitude($lng);
    }

    public function process(){
        return $this->geocoder
            ->geocode();
    }

    public function makeAddresses($response){
        $addresses = [];
        if($response['status']==='OK'){
            $response['results'] = array_slice($response['results'], 0, 8);
            foreach($response['results'] as $google_response_address){
                $address = Address::where('google_map_id',$google_response_address['place_id'])->first();
                if(!$address){
                    // If not exist in db -> create it
                    $address = new Address();
                    $address->google_map_id = $google_response_address['place_id'];
                    $address->lat = $google_response_address['geometry']['location']['lat'];
                    $address->lng = $google_response_address['geometry']['location']['lng'];
                    foreach($google_response_address['address_components'] as $address_component){
                        foreach($address_component['types'] as $type){
                            switch($type){
                                case "street_number":
                                    $address->number = $address_component['short_name'];
                                    break;
                                case "route":
                                    $address->street = $address_component['short_name'];
                                    break;
                                case "locality":
                                    $address->add_info = $address_component['short_name'];
                                    break;
                            }
                        }
                    }
                    // If address doesn't has street name - don't include it
                    if(!$address->street) break;
                    if(!$address->number) $address->number='';
                    $address->save();
                }
                $address->street = ($address->add_info?$address->add_info.', ':'') . $address->street;
                $addresses[] = $address;
            }
        }
        return $addresses;
    }
}