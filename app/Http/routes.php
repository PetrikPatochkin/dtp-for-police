<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/api/addresses', "AddressController@getAddresses");
$app->get('/api/routes', "RouteController@getRoutes");

$app->get('/accidents/update', "AccidentController@updateForm");
$app->post('/accidents/update', "AccidentController@update");
