<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;

class AccidentController extends Controller{
    public function updateForm(){
        return view('accident-form');
    }

    public function update(){
        $secret_word = Input::get('secret');
        if($secret_word !== '_UaPolice_') return 'Incorrect secret word';
        $file = Input::file('file');

        $array = [];
        $handle = fopen($file->getRealPath(), "r");
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $array[] = [
                'date' => date("Y-m-d H:i:s", strtotime($data[0])),
                'status' => trim($data[1])=='ДТП БЕЗ ПОТЕРПIЛИХ'?1:(trim($data[1])=='ДТП З ПОТЕРПIЛИМИ'?2:0) ,
                'lng' => floatval(preg_replace("/,/",".",$data[2])),
                'lat' => floatval(preg_replace("/,/",".",$data[3]))
            ];
        }
        //print_r($array);
        $insert_parts = array_chunk($array, 100);
        foreach($insert_parts as $insert_part){
            \DB::table('accidents')->insert($insert_part);
        }
        return "Success";
    }
}