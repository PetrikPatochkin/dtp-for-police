<?php

namespace App\Http\Controllers;

use App\Address;
use App\AddressesBuilder;
use Illuminate\Support\Facades\Input;


class AddressController extends Controller{

    public function getAddresses(){
        if($ids = Input::get('id')){
            $addresses = Address::whereIn('id',$ids)->get();
        } else {
            $addressesBuilder = new AddressesBuilder();
            if(Input::get('term'))
                $addressesBuilder->setAddress(Input::get('term'));
            elseif( Input::get('lat') && Input::get('lng') )
                $addressesBuilder->setCoordinates(Input::get('lat'), Input::get('lng'));

            $response = $addressesBuilder->process();
            $addresses = $addressesBuilder->makeAddresses($response);
        }

        $result = new \stdClass();
        $result->success = count($addresses)>0?'true':'false';
        $result->data = $addresses;
        return response()->json($result)->setCallback(Input::get('callback'));
    }
}