<?php

namespace App\Http\Controllers;

use App\RoutesBuilder;
use App\Address;
use Illuminate\Support\Facades\Input;

use Ivory\GoogleMap\Services\Directions\Directions;
use Ivory\GoogleMap\Services\Directions\DirectionsRequest;
use Ivory\GoogleMap\Services\Base\TravelMode;
use Ivory\GoogleMap\Services\Base\UnitSystem;
use Widop\HttpAdapter\CurlHttpAdapter;

class RouteController extends Controller{

    public function getRoutes(){

        $routeBuilder = new RoutesBuilder();
        $response = $routeBuilder->initKeyPoint(Input::get('addressId'));

        $routes = $routeBuilder->makeRoutes($response);

        $result = new \stdClass();
        $result->success = count($routes)>0?'true':'false';
        $result->data = $routes;
        return response()->json($result)->setCallback(Input::get('callback'));
    }


}