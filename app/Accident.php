<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Accident extends Model {
    public $timestamps = false;
    protected $visible = ['lat','lng','accidentsCount','victimsCount','deathsCount'];
}