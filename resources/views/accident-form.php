<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Updating accidents DB</title>
</head>
<body>
    <h2>Only .csv format with ";" delimeter</h2>
    <form action="" method="POST" enctype="multipart/form-data">
        <table>
            <tr>
                <td><input type="password" name="secret" placeholder="Secret word"/></td>
                <td><input type="file" name="file"/><br/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Update DB"/></td>
                <td></td>
            </tr>
        </table>
    </form>

    <h3>Example</h3>
    <table border="1">
        <tbody>
        <tr>
            <td>16.08.2015  11:51:00</td>
            <td>ДТП З ПОТЕРПIЛИМИ</td>
            <td>30,447578</td>
            <td>50,363122</td>
        </tr>
        <tr>
            <td>16.08.2015  12:42:00</td>
            <td>ДТП БЕЗ ПОТЕРПIЛИХ</td>
            <td>30,474755</td>
            <td>50,35768</td>
        </tr>
        </tbody>
    </table>
</body>
</html>